/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.andronicus.simplifiedrecyclerviewexample.util;

/**
 * Const.
 */
public class Const {
    /**
     * MINUTE
     */
    public static final long MINUTE = 60 * 1000L;

    /**
     * FLOAT_HALF
     */
    public static final float FLOAT_HALF = 0.5f;

    /**
     * KEY_DEFAULT
     */
    public static final String KEY_DEFAULT = "default";
    /**
     * KEY_INITIAL_PAGE
     */
    public static final String KEY_INITIAL_PAGE = "initial_page";

    /**
     * KEY_VIA_METHODS
     */
    public static final String KEY_VIA_METHODS = "via_methods";

    /**
     * KEY_VIA_LAYOUT
     */
    public static final String KEY_VIA_LAYOUT = "via_layout";

    /**
     * KEY_VIA_THEMES
     */
    public static final String KEY_VIA_THEMES = "via_themes";

    /**
     * KEY_WITH_LISTENER
     */
    public static final String KEY_WITH_LISTENER = "with_listener";

    /**
     * KEY_CENTER_CLICK_LISTENER
     */
    public static final String KEY_CENTER_CLICK_LISTENER = "center_click_listener";

    /**
     * KEY_BOTTOM
     */
    public static final String KEY_BOTTOM = "bottom";

    /**
     * KEY_TRIANGLE
     */
    public static final String KEY_TRIANGLE = "triangle";

    /**
     * KEY_NO_FADE
     */
    public static final String KEY_NO_FADE = "no_fade";

    /**
     * KEY_NO_180
     */
    public static final int KEY_NO_180 = 180;

    /**
     * KEY_NO_18
     */
    public static final int KEY_NO_18 = 10;

    /**
     * ZERO
     */
    public static final int ZERO = 0;

    /**
     * ONE
     */
    public static final int ONE = 1;

    /**
     * TWO
     */
    public static final int TWO = 2;

    /**
     * THREE
     */
    public static final int THREE = 3;

    /**
     * FOUR
     */
    public static final int FOUR = 4;

    /**
     * FIVE
     */
    public static final int FIVE = 5;

    /**
     * SIX
     */
    public static final int SIX = 6;

    /**
     * THIRTY
     */
    public static final int THIRTY = 30;

    /**
     * TWO50
     */
    public static final int TWO50 = 250;

    /**
     * RGB_COLOR
     */
    public static final int RGB_COLOR = 0xFFCCCCCC;

    /**
     * TWO70
     */
    public static final int TWO70 = 270;

    /**
     * TEN
     */
    public static final int TEN = 10;

    /**
     * FORMAT_HINT
     */
    public static final String FORMAT_HINT = "image/png";

    /**
     * FIFTY
     */
    public static final int FIFTY = 50;

    /**
     * SEVENTY
     */
    public static final int SEVENTY = 70;

    /**
     * TEN_TWNTY4
     */
    public static final int TEN_TWNTY4 = 1024;

    /**
     * MINUS_ONE
     */
    public static final int MINUS_ONE = -1;

    /**
     * FOURTY_EIGHT
     */
    public static final int FOURTY_EIGHT = 48;

    /**
     * HUNDRED
     */
    public static final int HUNDRED = 100;

    /**
     * TWENTY
     */
    public static final int TWENTY = 20;

    /**
     * TWELVE
     */
    public static final int TWELVE = 12;

    /**
     * FIFTEEN
     */
    public static final int FIFTEEN = 15;

    /**
     * HUNDRED
     */
    public static final int SixHundred = 600;

    private Const() {
    }
}

