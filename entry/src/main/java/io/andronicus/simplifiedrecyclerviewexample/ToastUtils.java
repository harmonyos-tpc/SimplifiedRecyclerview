/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.andronicus.simplifiedrecyclerviewexample;

import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * ToastUtils.
 */
public class ToastUtils {
    /**
     * showToast.
     *
     * @param content String
     * @param context Context
     */
    public static void showToast(Context context, String content) {
        new ToastDialog(context).setText(content).setDuration(1000).show();
    }
}
