/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.andronicus.simplifiedrecyclerviewexample.view;

import io.andronicus.simplifiedrecyclerview.MyAdapter;
import io.andronicus.simplifiedrecyclerviewexample.ResourceTable;
import io.andronicus.simplifiedrecyclerviewexample.ToastUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * SamplePageView.
 */
public class LongClickedRecycleItem extends AbstractPageView implements MyAdapter.ViewHolderCallbacks<String> {
    private MyAdapter<String> mAdapter;

    /**
     * SamplePageView.
     *
     * @param abilitySlice AbilitySlice
     * @param name         String
     * @param txtColor     Color
     */
    public LongClickedRecycleItem(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param name           String
     * @param iconId         integer
     * @param iconIdSelected integer
     * @param txtColor       Color
     */
    public LongClickedRecycleItem(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param txtColor       Color
     * @param redirectIntent AbilitySlice
     */
    public LongClickedRecycleItem(AbilitySlice abilitySlice, Color txtColor, AbilitySlice redirectIntent) {
        super(abilitySlice, txtColor, redirectIntent);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    /**
     * SamplePageView.
     *
     * @return layout Component
     */
    private Component loadView() {
        ComponentContainer layout = (ComponentContainer)
                LayoutScatter.getInstance(super.getSlice()).parse(ResourceTable.Layout_ability_main,
                        null, false);
        getData(layout);
        return layout;
    }

    private void getData(ComponentContainer layout) {
        /*
         * I have used a list of strings to simplify this
         * but your data should go here
         * */
        List<String> strings = new ArrayList<>();
        strings.add("0");
        strings.add("1");
        strings.add("2");
        strings.add("3");
        strings.add("4");
        strings.add("5");
        strings.add("6");
        strings.add("7");
        strings.add("8");
        strings.add("9");
        strings.add("10");
        strings.add("11");
        strings.add("12");
        strings.add("13");
        strings.add("14");
        strings.add("15");
        ListContainer recyclerView = (ListContainer) layout.findComponentById(ResourceTable.Id_listcontainer);
        recyclerView.setLayoutManager(new DirectionalLayoutManager());

        /*
         * Initialize your adapter and pass the required arguments
         * Make sure to implement MyAdapter.ViewHolderCallbacks<>
         * */
        mAdapter = new MyAdapter(ResourceTable.Layout_list_item, strings, getSlice().getContext(), this);
        recyclerView.setItemProvider(mAdapter);
        getAllItems();
        recyclerView.setItemLongClickedListener((listContainer, component, position, lval) -> {
            ToastUtils.showToast(getSlice().getContext(),
                    "Long Item clicked " + position);
            return false;
        });
    }

    private List<String> getAllItems() {
        return mAdapter.getAllItems();
    }

    private String getOneItem(int position) {
        return mAdapter.getItemAtPosition(position);
    }

    private void setData(List<String> data) {
        this.mAdapter.setData(data);
    }


    @Override
    public void onViewHolderClick(String item, int position) {
        ToastUtils.showToast(getSlice().getContext(),
                "clicked item " + position);
    }

    @Override
    public void onViewHolderLongClick(String item, int position) {
        ToastUtils.showToast(getSlice().getContext(),
                "clicked item " + position);
    }

    @Override
    public void bindDataToViews(String item, Component view) {
        Text textView = (Text) view.findComponentById(ResourceTable.Id_text_value);
        textView.setText(item);
    }
}


