/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.andronicus.simplifiedrecyclerviewexample.view.indicator;

import io.andronicus.simplifiedrecyclerviewexample.util.Const;
import io.andronicus.simplifiedrecyclerviewexample.util.ResUtil;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.app.Context;

/**
 * PageSliderIndicatorExt.
 */
public class PageSliderIndicatorExt extends ComponentContainer {
    private static final String TAG = PageSliderIndicatorExt.class.getSimpleName();
    private static final String INDICATOR_NORMAL_COLOR = "indicator_normalColor";
    private static final String INDICATOR_SELECTED_COLOR = "indicator_selectedColor";
    private static final String INDICATOR_STROKE_COLOR = "indicator_strokeColor";
    private static final String INDICATOR_NORMAL_STROKE_WIDTH = "indicator_normalStrokeWidth";
    private static final String INDICATOR_SELECTED_STROKE_WIDTH = "indicator_selectedStrokeWidth";

    private PageSliderIndicator mPageSliderIndicator;

    private int normalColor;
    private int selectedColor;
    private int strokeColor;
    private int normalStrokeWidth;
    private int selectedStrokeWidth;

    /**
     * PageSliderIndicatorExt.
     *
     * @param context Context
     * @param attrSet Attrset
     */
    public PageSliderIndicatorExt(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * PageSliderIndicatorExt.
     *
     * @param context Context
     * @param attrSet Attrset
     * @param styleName String
     */
    public PageSliderIndicatorExt(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initPageSliderIndicator(context, attrSet);
    }

    /**
     * PageSliderIndicatorExt.
     *
     * @param context Context
     * @param attrSet Attrset
     */
    private void initPageSliderIndicator(Context context, AttrSet attrSet) {
        mPageSliderIndicator = new PageSliderIndicator(context);
        DirectionalLayout.LayoutConfig indicatorLayoutConfig = new
                DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        indicatorLayoutConfig.alignment = LayoutAlignment.CENTER;
        mPageSliderIndicator.setLayoutConfig(indicatorLayoutConfig);
        addComponent(mPageSliderIndicator);

        if (attrSet.getAttr(INDICATOR_NORMAL_COLOR).isPresent()) {
            normalColor = attrSet.getAttr(INDICATOR_NORMAL_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_SELECTED_COLOR).isPresent()) {
            selectedColor = attrSet.getAttr(INDICATOR_SELECTED_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_STROKE_COLOR).isPresent()) {
            strokeColor = attrSet.getAttr(INDICATOR_STROKE_COLOR).get().getColorValue().getValue();
        }

        if (attrSet.getAttr(INDICATOR_NORMAL_STROKE_WIDTH).isPresent()) {
            normalStrokeWidth = attrSet.getAttr(INDICATOR_NORMAL_STROKE_WIDTH).get().getDimensionValue();
        }

        if (attrSet.getAttr(INDICATOR_SELECTED_STROKE_WIDTH).isPresent()) {
            selectedStrokeWidth = attrSet.getAttr(INDICATOR_SELECTED_STROKE_WIDTH).get().getDimensionValue();
        }
    }

    /**
     * PageSliderIndicatorExt.
     *
     * @param pageSlider PageSlider
     */
    public void setPageSlider(PageSlider pageSlider) {
        mPageSliderIndicator.setPageSlider(pageSlider);
    }

    /**
     * PageSliderIndicatorExt.
     *
     * @param offset setItemOffset
     */
    public void setItemOffset(int offset) {
        mPageSliderIndicator.setItemOffset(offset);
    }

    /**
     * setItemElement.
     *
     * @param normal Element
     * @param selected Element
     */
    public void setItemElement(Element normal, Element selected) {
        mPageSliderIndicator.setItemElement(normal, selected);
    }

    /**
     * setTabStyleViaLayout.
     *
     */
    public void setTabStyleViaLayout() {
        setItemElement(
                ResUtil.getCustomRectGradientDrawable(new Color(normalColor),
                        new Rect(0, 0, Const.SixHundred, Const.TWO)),
                ResUtil.getCustomRectGradientDrawable(new Color(selectedColor),
                        new Rect(0, 0, Const.SixHundred, Const.TEN)));
    }
}
