# SimplifiedRecyclerview
  SimplifiedRecyclerview: An openharmony library for panning an image horizontal and vertically.
 
## SimplifiedRecyclerview includes:
An openharmony library to help you get rid of boiler plate code when setting up Recyclerview and ListContainer

## Usage Instructions
1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.

2. The following core classes are the essential interface to SimplifiedRecyclerview:

 MyAdapter : To be able to initialize MyAdapter, you will need 3 arguments, the layout id of Recyclerview 
viewholder list item, list of items you are interested in displaying and lastly implementation of 
ViewHolderCallbacks to handle binding data and view holder click events.

3. The steps to initialize the SimplifiedRecyclerview and the core classes:
   
       @Override
       public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getData();
    }

       private void getData() {
           /*
            * I have used a list of strings to simplify this
            * but your data should go here
            * */
        List<String> strings = new ArrayList<>();
        strings.add("0");
        strings.add("1");
        strings.add("2");
        strings.add("3");
        strings.add("4");
        strings.add("5");
        strings.add("6");
        strings.add("7");
        strings.add("8");
        strings.add("9");
        strings.add("10");
        strings.add("11");
        strings.add("12");
        strings.add("13");
        strings.add("14");
        strings.add("15");
        ListContainer recyclerView = (ListContainer) findComponentById(ResourceTable.Id_listcontainer);
        recyclerView.setLayoutManager(new DirectionalLayoutManager());

        /*
         * Initialize your adapter and pass the required arguments
         * Make sure to implement MyAdapter.ViewHolderCallbacks<>
         * */
        mAdapter = new MyAdapter<>(ResourceTable.Layout_list_item, strings, this, this);
        recyclerView.setItemProvider(mAdapter);
		
  We passed (*this) as a context for the 3rd argument above because MainActivity implements
  ViewHolderCallbacks as shown below.
  
	public class MainAbility extends Ability implements MyAdapter.ViewHolderCallbacks<String> {
    private MyAdapter<String> mAdapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getData();
    }

        private void getData() {
        /*
         * I have used a list of strings to simplify this
         * but your data should go here
         * */
        List<String> strings = new ArrayList<>();
        strings.add("0");
        strings.add("1");
        strings.add("2");
        strings.add("3");
        strings.add("4");
        strings.add("5");
        strings.add("6");
        strings.add("7");
        strings.add("8");
        strings.add("9");
        strings.add("10");
        strings.add("11");
        strings.add("12");
        strings.add("13");
        strings.add("14");
        strings.add("15");
        ListContainer recyclerView = (ListContainer) findComponentById(ResourceTable.Id_listcontainer);
        recyclerView.setLayoutManager(new DirectionalLayoutManager());

        /*
         * Initialize your adapter and pass the required arguments
         * Make sure to implement MyAdapter.ViewHolderCallbacks<>
         * */
        mAdapter = new MyAdapter<>(ResourceTable.Layout_list_item, strings, this, this);
        recyclerView.setItemProvider(mAdapter);
        getAllItems();
       
    }

    private List<String> getAllItems() {
        return mAdapter.getAllItems();
    }

    private String getOneItem(int position) {
        return mAdapter.getItemAtPosition(position);
    }

    private void setData(List<String> data) {
        this.mAdapter.setData(data);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onViewHolderClick(String item, int position) {
        ToastUtils.showToast(MainAbility.this,
                "clicked item " + position);
    }

    @Override
    public void onViewHolderLongClick(String item, int position) {
        ToastUtils.showToast(MainAbility.this,
                "clicked item " + position);
    }

    @Override
    public void bindDataToViews(String item, Component view) {
        Text textView = (Text) view.findComponentById(ResourceTable.Id_text_value);
        textView.setText(item);
    }

 ## Installation instruction

1. For using simplifiedrecyclerview module in sample app,include the below library dependency to generate hap/simplifiedrecyclerview.har:
Add the dependencies in entry/build.gradle as below :

    dependencies {
         implementation project(path: ':simplifiedrecyclerview')
      }

2. Using the simplifiedrecyclerview har, make sure to add simplifiedrecyclerview.har file in the entry/libs folder and add the below dependency
in build.gradle.
Modify the dependencies in the entry/build.gradle file.

        dependencies {
              implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
          }
         
3. For using SimplifiedRecyclerview from a remote repository in separate application, add the below dependency in entry/build.gradle file.
 
 Modify entry build.gradle as below :
             
             
             ```gradle
             dependencies {
                    implementation 'io.openharmony.tpc.thirdlib:SimplifiedRecyclerview:1.0.0'
             }
             ```
##Credits
Lead Engineer - Andronicus Kim(@andronicus_kim7)

 ## License
MIT License

Copyright (c) 2018 Andronicus

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation the 
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 copies of the Software, and to permit persons to whom the Software is furnished to
 do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies
 or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

